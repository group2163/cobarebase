import java.util.Scanner;

public class OddNumber {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("nilai awal : ");
        int awal = in.nextInt();
        System.out.print("nilai akhir : ");
        int akhir = in.nextInt();
        for(int i=awal; i<=akhir; i++){
            if(i%2==1){
                System.out.print(i+ " ");
            }
        }
    }
}
